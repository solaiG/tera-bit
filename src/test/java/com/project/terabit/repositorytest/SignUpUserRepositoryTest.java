package com.project.terabit.repositorytest;

import java.math.BigInteger;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.entity.UserEntity;
import com.project.terabit.model.User;
import com.project.terabit.repositroy.UserRepository;
import com.project.terabit.service.SignUpUserServiceImpl;
import com.project.terabit.utility.PasswordHashing;

public class SignUpUserRepositoryTest extends TerabitApplicationTests {
	
	@Mock
	UserRepository userrepository;
	
	@InjectMocks
	SignUpUserServiceImpl signupservice;
	
	@InjectMocks
	PasswordHashing passwordHasher;
	
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    
    @Test
    public  void checkUserException() throws Exception{
    	expectedException.expect(Exception.class);
        expectedException.expectMessage("SERVICE.invalid_signUp");
        UserEntity userentity = new UserEntity();
        User user = new User();
        user.setUserEmailId("lisadeepik@gmail.com");
        user.setUserContactNo(BigInteger.valueOf(7639312932l));
        user.setUserPassword("Deepik@lisa79");
        Mockito.when(userrepository.checkUser("lisadeepik@gmail.com", BigInteger.valueOf(7639312932l))).thenReturn(userentity);
        signupservice.saveUser(user);     
    }
   
    @Test
    public void checkUserSuccess() throws Exception{
        User user = new User();
        user.setUserFirstName("lisa");
        user.setUserLastName("Deepik");
        user.setUserEmailId("lisadeepik@gmail.com");
        user.setUserContactNo(BigInteger.valueOf(7639312932l));
        user.setUserPassword("Deepik@lisa79");
        Mockito.when(userrepository.checkUser("lisadeepik@gmail.com", BigInteger.valueOf(7639312932l))).thenReturn(null);
        signupservice.saveUser(user); 
    }

}
