package com.project.terabit.model;

import java.math.BigInteger;
import java.time.LocalDateTime;


/**
 * The Class ViewedProperty.
 */
public class ViewedProperty {
	
	/** The viewed property id. */
	private BigInteger viewedPropertyId;

	/** The viewed property property id. */
	private BigInteger viewedPropertyPropertyId;

	/** The viewed user id. */
	private BigInteger viewedUserId;

	/** The viewed time. */
	private LocalDateTime viewedTime;

	/** The viewed seller id. */
	private BigInteger viewedSellerId;

	/**
	 * Gets the viewed property id.
	 *
	 * @return the viewed property id
	 */
	public BigInteger getViewedPropertyId() {
		return viewedPropertyId;
	}

	/**
	 * Sets the viewed property id.
	 *
	 * @param viewedPropertyId the new viewed property id
	 */
	public void setViewedPropertyId(BigInteger viewedPropertyId) {
		this.viewedPropertyId = viewedPropertyId;
	}

	/**
	 * Gets the viewed property property id.
	 *
	 * @return the viewed property property id
	 */
	public BigInteger getViewedPropertyPropertyId() {
		return viewedPropertyPropertyId;
	}

	/**
	 * Sets the viewed property property id.
	 *
	 * @param viewedPropertyPropertyId the new viewed property property id
	 */
	public void setViewedPropertyPropertyId(BigInteger viewedPropertyPropertyId) {
		this.viewedPropertyPropertyId = viewedPropertyPropertyId;
	}

	/**
	 * Gets the viewed user id.
	 *
	 * @return the viewed user id
	 */
	public BigInteger getViewedUserId() {
		return viewedUserId;
	}

	/**
	 * Sets the viewed user id.
	 *
	 * @param viewedUserId the new viewed user id
	 */
	public void setViewedUserId(BigInteger viewedUserId) {
		this.viewedUserId = viewedUserId;
	}

	/**
	 * Gets the viewed time.
	 *
	 * @return the viewed time
	 */
	public LocalDateTime getViewedTime() {
		return viewedTime;
	}

	/**
	 * Sets the viewed time.
	 *
	 * @param viewedTime the new viewed time
	 */
	public void setViewedTime(LocalDateTime viewedTime) {
		this.viewedTime = viewedTime;
	}

	/**
	 * Gets the viewed seller id.
	 *
	 * @return the viewed seller id
	 */
	public BigInteger getViewedSellerId() {
		return viewedSellerId;
	}

	/**
	 * Sets the viewed seller id.
	 *
	 * @param viewedSellerId the new viewed seller id
	 */
	public void setViewedSellerId(BigInteger viewedSellerId) {
		this.viewedSellerId = viewedSellerId;
	}
	
	
}
