package com.project.terabit.controller;


import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.User;
import com.project.terabit.service.LoginUserServiceImpl;




/**
 * The Class LoginController.
 */
@RestController
@RequestMapping(value="/terabit/api/v1/user")
public class LoginController {
	
	/** The log. */
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	
	/** The loginservice. */
	@Autowired(required=true)
	private LoginUserServiceImpl loginservice;
	
	/** The prop. */
	Properties prop = new Properties(); 
	
	/** The input stream. */
	InputStream inputStream = getClass().getClassLoader().getResourceAsStream("application.properties");
	
	/**
	 * User login.
	 *
	 * @param userModel the user model
	 * @return the user
	 * @throws Exception the exception
	 */
	@GetMapping(value="/login")
	public User userLogin(@RequestBody User userModel) throws Exception {
		
		User returnUserModel = new User();
		String response=null;
		try {
			returnUserModel=loginservice.login(userModel);
			response="Success";
			returnUserModel.setMessage(response);
		}catch(Exception e) {
			returnUserModel=new User();
			prop.load(inputStream); 
			returnUserModel.setMessage(response+prop.getProperty(e.getMessage()));
			String s=response+prop.getProperty(e.getMessage());
			log.error(s);
			
		}
		return returnUserModel;
		
	}
}
