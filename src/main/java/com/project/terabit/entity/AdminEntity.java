package com.project.terabit.entity;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;



/**
 * The Class AdminEntity.
 */
@Entity
@Table(name="admin")
public class AdminEntity {
	
	/** The admin id. */
	@Id
	@GeneratedValue
	@NotNull
	@Column(name="admin_id")
	private BigInteger adminId;
	
	/** The admin seller id. */
	
	@Column(name="admin_seller_id")
	private BigInteger adminSellerId;
	
	/** The admin rights by. */
	
	@Column(name="admin_rights_by")
	private BigInteger adminRightsBy;
	
	/** The admin is active. */
	@NotNull
	@Column(name="admin_is_active")
	private boolean adminIsActive=false;
	
	/** The admin created time. */
	@NotNull
	@Column(name="admin_created_time")
	private LocalDateTime adminCreatedTime;
	
	/** The admin seller count. */
	@NotNull
	@Column(name="admin_seller_count")
	private BigInteger adminSellerCount;	
	
	/** The admin notification id. */
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	private List<NotificationEntity> adminNotificationIds = new ArrayList<>();
	
	/** The admin feedback id. */
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true,targetEntity=com.project.terabit.entity.FeedbackEntity.class)
	private List<FeedbackEntity> adminFeedbackIds = new ArrayList<>();
	
	/**
	 * Gets the admin id.
	 *
	 * @return the admin id
	 */
	public BigInteger getAdminId() {
		return adminId;
	}

	/**
	 * Sets the admin id.
	 *
	 * @param adminId the new admin id
	 */
	public void setAdminId(BigInteger adminId) {
		this.adminId = adminId;
	}

	/**
	 * Gets the admin seller id.
	 *
	 * @return the admin seller id
	 */
	public BigInteger getAdminSellerId() {
		return adminSellerId;
	}

	/**
	 * Sets the admin seller id.
	 *
	 * @param adminSellerId the new admin seller id
	 */
	public void setAdminSellerId(BigInteger adminSellerId) {
		this.adminSellerId = adminSellerId;
	}

	/**
	 * Gets the admin rights by.
	 *
	 * @return the admin rights by
	 */
	public BigInteger getAdminRightsBy() {
		return adminRightsBy;
	}

	/**
	 * Sets the admin rights by.
	 *
	 * @param adminRightsBy the new admin rights by
	 */
	public void setAdminRightsBy(BigInteger adminRightsBy) {
		this.adminRightsBy = adminRightsBy;
	}

	/**
	 * Checks if is admin is active.
	 *
	 * @return true, if is admin is active
	 */
	public boolean isAdminIsActive() {
		return adminIsActive;
	}

	/**
	 * Sets the admin is active.
	 *
	 * @param adminIsActive the new admin is active
	 */
	public void setAdminIsActive(boolean adminIsActive) {
		this.adminIsActive = adminIsActive;
	}

	/**
	 * Gets the admin created time.
	 *
	 * @return the admin created time
	 */
	public LocalDateTime getAdminCreatedTime() {
		return adminCreatedTime;
	}

	/**
	 * Sets the admin created time.
	 *
	 * @param adminCreatedTime the new admin created time
	 */
	public void setAdminCreatedTime(LocalDateTime adminCreatedTime) {
		this.adminCreatedTime = adminCreatedTime;
	}

	/**
	 * Gets the admin seller count.
	 *
	 * @return the admin seller count
	 */
	public BigInteger getAdminSellerCount() {
		return adminSellerCount;
	}

	/**
	 * Sets the admin seller count.
	 *
	 * @param adminSellerCount the new admin seller count
	 */
	public void setAdminSellerCount(BigInteger adminSellerCount) {
		this.adminSellerCount = adminSellerCount;
	}

	/**
	 * Gets the admin notification id.
	 *
	 * @return the admin notification id
	 */
	public List<NotificationEntity> getAdminNotificationId() {
		return adminNotificationIds;
	}

	/**
	 * Sets the admin notification id.
	 *
	 * @param adminNotificationIds the new admin notification id
	 */
	public void setAdminNotificationId(List<NotificationEntity> adminNotificationIds) {
		this.adminNotificationIds = adminNotificationIds;
	}

	/**
	 * Gets the admin feedback id.
	 *
	 * @return the admin feedback id
	 */
	public List<FeedbackEntity> getAdminFeedbackId() {
		return adminFeedbackIds;
	}

	/**
	 * Sets the admin feedback id.
	 *
	 * @param adminFeedbackIds the new admin feedback id
	 */
	public void setAdminFeedbackId(List<FeedbackEntity> adminFeedbackIds) {
		this.adminFeedbackIds = adminFeedbackIds;
	}
	
	
}
