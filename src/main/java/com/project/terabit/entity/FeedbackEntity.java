package com.project.terabit.entity;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


/**
 * The Class FeedbackEntity.
 */
@Entity
@Table(name="feedback")
public class FeedbackEntity {
	
	/** The feedback id. */
	@Id
	@GeneratedValue
	@NotNull
	@Column(name="feedback_id")
	private BigInteger feedbackId;
	
	/** The feedback rating. */
	@NotNull
	@Column(name="feedback_rating")
	private int feedbackRating;
	
	/** The feedback description. */
	@Column(name="feedback_description")
	private String feedbackDescription;
	
	/** The feedback given by. */
	@Column(name="feedback_given_by")
	private BigInteger feedbackGivenBy;
	
	/** The feedback created by. */
	@Column(name="feedback_created_by")
	private String feedbackCreatedBy;

	/**
	 * Gets the feedback id.
	 *
	 * @return the feedback id
	 */
	public BigInteger getFeedbackId() {
		return feedbackId;
	}

	/**
	 * Sets the feedback id.
	 *
	 * @param feedbackId the new feedback id
	 */
	public void setFeedbackId(BigInteger feedbackId) {
		this.feedbackId = feedbackId;
	}

	/**
	 * Gets the feedback rating.
	 *
	 * @return the feedback rating
	 */
	public int getFeedbackRating() {
		return feedbackRating;
	}

	/**
	 * Sets the feedback rating.
	 *
	 * @param feedbackRating the new feedback rating
	 */
	public void setFeedbackRating(int feedbackRating) {
		this.feedbackRating = feedbackRating;
	}

	/**
	 * Gets the feedback description.
	 *
	 * @return the feedback description
	 */
	public String getFeedbackDescription() {
		return feedbackDescription;
	}

	/**
	 * Sets the feedback description.
	 *
	 * @param feedbackDescription the new feedback description
	 */
	public void setFeedbackDescription(String feedbackDescription) {
		this.feedbackDescription = feedbackDescription;
	}

	/**
	 * Gets the feedback created by.
	 *
	 * @return the feedback created by
	 */
	public String getFeedbackCreatedBy() {
		return feedbackCreatedBy;
	}

	/**
	 * Sets the feedback created by.
	 *
	 * @param feedbackCreatedBy the new feedback created by
	 */
	public void setFeedbackCreatedBy(String feedbackCreatedBy) {
		this.feedbackCreatedBy = feedbackCreatedBy;
	}

	/**
	 * Gets the feedback given by.
	 *
	 * @return the feedback given by
	 */
	public BigInteger getFeedbackGivenBy() {
		return feedbackGivenBy;
	}

	/**
	 * Sets the feedback given by.
	 *
	 * @param feedbackGivenBy the new feedback given by
	 */
	public void setFeedbackGivenBy(BigInteger feedbackGivenBy) {
		this.feedbackGivenBy = feedbackGivenBy;
	}
	
	

}
